package kamar.ali.elasticsearchexample.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class SongDto {
    private String name;
    private int trackNumber;
}
