package kamar.ali.elasticsearchexample.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class AlbumDto {
    @Id
    private String id;
    private String name;
    private String artist;
    private String release;
    private List<SongDto> songDtoList;
}
