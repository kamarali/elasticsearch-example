package kamar.ali.elasticsearchexample.service;

import kamar.ali.elasticsearchexample.domain.AlbumDto;
import kamar.ali.elasticsearchexample.repository.MusicRepository;
import kamar.ali.elasticsearchexample.utilities.LoggingUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MusicService {

    private LoggingUtility loggingUtility;
    private MusicRepository musicRepository;

    @Autowired
    private  MusicService(LoggingUtility loggingUtility, MusicRepository musicRepository) {
        this.loggingUtility = loggingUtility;
        this.musicRepository = musicRepository;
    }

    public ResponseEntity newAlbum(AlbumDto albumDto) {
        loggingUtility.logInfo(MusicService.class.getSimpleName(),
                String.format("Creating new album: %s - %s", albumDto.getName(), albumDto.getArtist()));

        try {
            AlbumDto savedAlbum = musicRepository.save(albumDto);
            return new ResponseEntity(savedAlbum, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(String.format("Unable to save album\n%s", e), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity updateAlbum(AlbumDto albumDto) {
        loggingUtility.logInfo(MusicService.class.getSimpleName(),
                String.format("Updating album: %s - %s", albumDto.getName(), albumDto.getArtist()));

        try {
            Optional optionalCollectedAlbum = musicRepository.findById(albumDto.getId());
            HttpStatus responseStatus = getResponseStatus(optionalCollectedAlbum);

            if(responseStatus == HttpStatus.NOT_FOUND) {
                return new ResponseEntity(String.format("Album with ID %s not found", albumDto.getId()), responseStatus);
            }

            AlbumDto savedAlbum = musicRepository.save(albumDto);
            return new ResponseEntity(savedAlbum, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(String.format("Unable to save album\n%s", e), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity getAlbum(String id) {
        loggingUtility.logInfo(MusicService.class.getSimpleName(), String.format("Getting album with ID: %s", id));

        Optional optionalCollectedAlbum = musicRepository.findById(id);
        HttpStatus responseStatus = getResponseStatus(optionalCollectedAlbum);

        if(responseStatus == HttpStatus.NOT_FOUND) {
            return new ResponseEntity(String.format("Album with ID %s not found", id), responseStatus);
        }

        return new ResponseEntity(optionalCollectedAlbum.get(), responseStatus);
    }

    public ResponseEntity deleteAlbum(String id) {
        loggingUtility.logInfo(MusicService.class.getSimpleName(), String.format("Deleting album with ID: %s", id));

        Optional optionalCollectedAlbum = musicRepository.findById(id);
        HttpStatus responseStatus = getResponseStatus(optionalCollectedAlbum);

        if(responseStatus == HttpStatus.NOT_FOUND) {
            return new ResponseEntity(String.format("Album with ID %s not found", id), responseStatus);
        }

        musicRepository.deleteById(id);
        return new ResponseEntity(String.format("Successfully removed album with ID:%s", id), responseStatus);
    }

    private HttpStatus getResponseStatus(Optional<AlbumDto> optionalAlbum) {
        if(optionalAlbum.isPresent()) {
            return HttpStatus.OK;
        }

        return HttpStatus.NOT_FOUND;
    }
}