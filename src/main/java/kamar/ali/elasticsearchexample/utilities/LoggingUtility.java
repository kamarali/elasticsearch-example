package kamar.ali.elasticsearchexample.utilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class LoggingUtility {
    private final Logger LOGGER = LoggerFactory.getLogger(LoggingUtility.class);

    public void logInfo(String component, String message) {
        LOGGER.info("[{}]: {}", component, message);
    }
}
