package kamar.ali.elasticsearchexample.controller;

import kamar.ali.elasticsearchexample.domain.AlbumDto;
import kamar.ali.elasticsearchexample.service.MusicService;
import kamar.ali.elasticsearchexample.utilities.LoggingUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/music/")
public class MusicController {
    private LoggingUtility loggingUtility;
    private MusicService musicService;

    @Autowired
    private MusicController(LoggingUtility loggingUtility, MusicService musicService) {
        this.loggingUtility = loggingUtility;
        this.musicService = musicService;
    }

    @PostMapping(path = "album", consumes = "application/json", produces = "application/json")
    public ResponseEntity newAlbum(@RequestBody AlbumDto albumDto) {
        loggingUtility.logInfo(MusicController.class.getSimpleName(), String.format("POST /music/album"));
        return musicService.newAlbum(albumDto);
    }

    @PutMapping(path = "album", consumes = "application/json", produces = "application/json")
    public ResponseEntity updateAlbum(@RequestBody AlbumDto albumDto) {
        loggingUtility.logInfo(MusicController.class.getSimpleName(), String.format("PUT /music/album"));
        return musicService.updateAlbum(albumDto);
    }

    @GetMapping(path = "album/{id}", produces = "application/json")
    public ResponseEntity getAlbum(@PathVariable("id") String id) {
        loggingUtility.logInfo(MusicController.class.getSimpleName(), String.format("GET /music/album/%s", id));
        return musicService.getAlbum(id);
    }

    @DeleteMapping(path = "album/{id}", produces = "application/json")
    public ResponseEntity deleteAlbum(@PathVariable("id") String id) {
        loggingUtility.logInfo(MusicController.class.getSimpleName(), String.format("DELETE /music/album/%s", id));
        return musicService.deleteAlbum(id);
    }
}