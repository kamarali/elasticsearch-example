package kamar.ali.elasticsearchexample.repository;

import kamar.ali.elasticsearchexample.domain.AlbumDto;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MusicRepository extends MongoRepository<AlbumDto, String> {
}
