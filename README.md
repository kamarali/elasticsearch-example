Make sure you have docker and maven installed.

This will spin up the environment locally:
`./scripts/buildAndDeployContainersLocal.sh`

To bring the environment down:
`docker-compose down`

Use the `elasticSearchExample.postman_collection.json` file for the four REST methods (GET, POST, PUT & DELETE)