FROM anapsix/alpine-java
MAINTAINER Kamar Ali "kamarali@hotmail.co.uk"
ARG profile
COPY ./target/elasticsearch-example-0.0.1-SNAPSHOT.jar /app.jar
EXPOSE 8080
CMD ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]